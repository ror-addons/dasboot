DasBoot = {}

baseTime = 0

days_no_vacation = 14
days_with_vacation = 45
max_status_number = 2

DasBoot.BootList = {}

local m_bListPrinted = false		

local function print(txt)
	EA_ChatWindow.Print(towstring(txt))
end

function DasBoot.Initialize()
	LibSlash.RegisterSlashCmd("dasboot", function() DasBoot.Info() end)
	LibSlash.RegisterSlashCmd("dasmaxdays", function(max) DasBoot.setMaxDays(max) end)
	LibSlash.RegisterSlashCmd("dasmaxvacation", function(max) DasBoot.setMaxVacation(max) end)
	LibSlash.RegisterSlashCmd("dasmaxlevel", function(max) DasBoot.setMaxLevel(max) end)
	LibSlash.RegisterSlashCmd("dasprint", function() DasBoot.PrintList() end)
	LibSlash.RegisterSlashCmd("daspurge", function() DasBoot.PurgeList() end)
end

function DasBoot.Info()
	print("Das Boot:")
	print("/dasmaxdays [#days] - to set max days WITHOUT vacation")
	print("/dasmaxvacation [#days] - to set max days if word 'vacation' is in notes")
	print("/dasmaxlevel [#level] - to set max guild level to boot. Will not boot people above this level.")
	print("/dasprint -- print the names of all people to boot")
	print("/daspurge -- start the booting")
	print("Current Settings:")
	print("Days no vacation:"..tostring(days_no_vacation))
	print("Days with vacation:"..tostring(days_with_vacation))
	print("Maxium Level to Boot:"..tostring(max_status_number))
end

function DasBoot.PurgeList()
	if m_bListPrinter == false then
		print("You must print the list first to review it before you can purge.")
		return
	end

	SystemData.UserInput.ChatText = towstring("/guild Das Boot: Kicking inactive members from the guild. Be sure to put 'vacation' in your note if you are on vacation")
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
	for _,member in pairs(DasBoot.BootList) do
			bootee = member.name:match(L"([^^]+)^?.*")
			SystemData.UserInput.ChatText = towstring("/guildkick " .. bootee)
			BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
	end
	
	m_bListPrinter = false
	DasBoot.BootList = {}
end

	
	
function DasBoot.setMaxDays(max)
	days_no_vacation = tonumber(max)
	DasBoot.Info()
end

function DasBoot.setMaxVacation(max)
	days_with_vacation = tonumber(max)
	DasBoot.Info()
end

function DasBoot.setMaxLevel(max)
	level = tonumber(max) 
	if (level>8 or level < 0) then
		print("Max Level must be between 0 and 8")
		return
	end
	max_status_number = level
	DasBoot.Info()
end


function DasBoot.PrintList()
	DasBoot.BuildList()
	for _,member in pairs(DasBoot.BootList) do
		print(member.name)
	end
	m_bListPrinted = true
end


function DasBoot.ChatPrint(msg)
	SystemData.UserInput.ChatText = towstring("/as " .. msg)
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function DasBoot.getBaseTime()
	guildMembers = GetGuildMemberData()
	
	for _, member in pairs (guildMembers) do
		local name = member.name:match(L"([^^]+)^?.*")
		if name == GameData.Player.name:match(L"([^^]+)^?.*") then
			return member.lastLogin
		end
	end
end

function DasBoot.BuildList()
	DasBoot.BootList = {}
	baseTime = DasBoot.getBaseTime()
	guildMembers = GetGuildMemberData()
	
	for _, member in pairs (guildMembers) do
		if member.note == nil then
			note = L""
		else
			note = wstring.upper(member.note)
			note = towstring(note)
		end
		
		local onVac = wstring.find(note,L"VACATION") or 0
		if onVac > 0 then
			day_limit = days_with_vacation
		else
			day_limit = days_no_vacation
		end
		
		if member.statusNumber <= max_status_number then
		
			if (member.lastLogin + (day_limit*24*60*60)) < baseTime then
				table.insert(DasBoot.BootList, member)
			end
		end
	end
end

	
	