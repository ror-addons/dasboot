<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="DasBoot" version="0.1" date="4/8/2009" >

		<Author name="Illadrel" email="" />
		<Description text="Boots inactive guild members" />
		
		<Dependencies>
		    <Dependency name="LibSlash" />
        	</Dependencies>
        	
		<Files>
			<File name="DasBoot.lua" />
		</Files>
	
		<OnInitialize>
			<CallFunction name="DasBoot.Initialize" />
		</OnInitialize>
		
		<OnShutdown/>
	</UiMod>
</ModuleFile>
